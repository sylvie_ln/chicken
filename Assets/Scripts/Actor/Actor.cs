﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Actor is a generic class for game objects that need to move around and collide with each other.
 * The main reason to use this class is to use its MoveAndCollide method.
 * This method lets you move an Actor around and have it correctly respond to collisions with other game objects.
 * It also keeps the Actor's position aligned with the pixel grid, while still allowing movement to fractional positions (like "4.7 pixels").
 * The fractional position is tracked internally, while the pixel grid position is used for collision and rendering.
 * Keeping Actors locked to the pixel grid makes collision logic simpler and avoids graphical glitches and anomalies.
 */
[RequireComponent(typeof(BoxCollider2D))]
public class Actor : MonoBehaviour {
    /** 
     * The position of the actor on the pixel grid.
     */
    [HideInInspector]
    public Vector3Int pixelPosition;
    /** 
     * The offset of the actor from the pixel grid. This offset is used to allow Actors to move at fractional speed values. 
     * The subpixel offset is in the following range: -0.5 < subpixel offset <= 0.5.
     */
    [HideInInspector]
    public Vector3 subpixelOffset;
    // Added together, the pixel position and the subpixel offset give the "true position" of the Actor.
    // The pixel position is always the rounded version of the true position, where numbers with a fractional part of 0.5 are rounded down.
    // The subpixel offset is always the true position minus the pixel position.
    // For example, if true horizontal position of the Actor is 4.7 pixels, the horizontal pixel position is 5 and the horizontal subpixel offset is -0.3.

    /**
     * These variables store the boundaries of the Actor's hitbox.
     * They are absolute positions rather than relative.
     * For example, if the actor is at horizontal pixel position 32, and the left side of the hitbox is at horizontal pixel position 24,
     * then the variable hitboxLeft would have value 24 rather than value -8.
     */
    [HideInInspector]
    public int hitboxLeft, hitboxRight, hitboxTop, hitboxBottom;

    // The actual collider representing the Actor's hitbox.
    private BoxCollider2D bc;

    /**
     * Initialize the actor.
     */
    protected virtual void Start() {
        // Initialize the pixel position, hitbox boundaries, and subpixel offset of the actor.
        bc = GetComponent<BoxCollider2D>();
        pixelPosition = new Vector3Int(RoundTiesDown(transform.position.x), RoundTiesDown(transform.position.y), 0);
        subpixelOffset = transform.position - pixelPosition;
        ComputeHitbox();
    }

    /**
     * Move the Actor by the given amount along the given axis, ignoring collisions.
     */
    public void Move(float amount, int axis) {
        // Determine the target position.
        float targetPosition = pixelPosition[axis] + subpixelOffset[axis] + amount;
        // Round it to get the target pixel position, and update the pixel position.
        pixelPosition[axis] = RoundTiesDown(targetPosition);
        // Update the subpixel offset: it's the difference between the target position and our new pixel position.
        subpixelOffset[axis] = targetPosition - pixelPosition[axis];

        // We've moved, so recompute our hitbox boundaries and sync pixel position with transform position.
        ComputeHitbox();
        UpdateTransform();
    }

    /**
     * Move the Actor by the given amount along the given axis, handling collisions appropriately.
     * This function depends on two other functions: CheckCollisions and HandleCollisions.
     * The Actor is moved in one-pixel steps, using CheckCollisions to see if a step will cause a collision will occur.
     * When collisions are found, they are processed using HandleCollisions.
     * HandleCollisions returns a boolean value that determines if the Actor should stop (e.g., if a wall was hit)
     * or keep moving (e.g., if a collectable object was hit).
     * You can control how each Actor deals with collisions and what kinds of objects they collide with by overriding
     * CheckCollisions and HandleCollisions.
     * Your can optionally pass in a layer mask to only check for collisions against certain layers.
     * For example, if you have a layer called "Solids" you can pass in LayerMask.GetMask("Solids") as the second argument.
     * This function returns false if the Actor's movement was stopped prematurely, and true otherwise.
     */
    public virtual bool MoveAndCollide(float amount, int axis, int layerMask = Physics2D.AllLayers) {
        // Determine the target position.
        float targetPosition = pixelPosition[axis] + subpixelOffset[axis] + amount;
        // Round it to get the target pixel position.
        int targetPixelPosition = RoundTiesDown(targetPosition);
        // The pixel amount we want to move is the difference between the target position and current position.
        int pixelAmount = targetPixelPosition - pixelPosition[axis];
        // Update the subpixel offset: it's the difference between the target position and the target pixel position.
        subpixelOffset[axis] = targetPosition - targetPixelPosition;

        // Set up variables related to stepping.
        int stepAmount = System.Math.Sign(amount);
        Vector3Int stepVector = Vector3Int.zero;
        stepVector[axis] = stepAmount;
        // We return false if our movement is stopped prematurely by a collision, and true otherwise.
        bool moveCompleted = true;

        while (pixelAmount != 0) {
            // Check for collisions one step ahead.
            List<Collider2D> colliders = CheckCollisions(stepVector, layerMask);
            if (colliders.Count != 0) { // If we hit something...
                bool stop = HandleCollisions(colliders); // Handle the collisions and figure out whether we should stop
                if (stop) { // If we hit something that should make the actor stop...
                    // Break out of the loop and return false to indicate that we were stopped.
                    moveCompleted = false;
                    break;
                }
            }
            // Now, we actually take the step (update the pixel position).
            pixelPosition += stepVector;
            // Decrease the pixel amount left to move.
            pixelAmount -= stepAmount;

            // Because the character moved, we need to recompute the hitbox.
            ComputeHitbox();
        }
        // The character is done moving, so sync the transform position with the pixel position.
        // Why don't we call this in the loop? Well, it's because we check for colliders by comparing
        // the hitbox variables of this Actor with other colliders, rather than using the Actor's transform. 
        // No Actors other than this one are moving around in the loop, so we don't need to resync transforms, 
        // we can just keep hitbox variables updated. If you modify this method to do more complicated things, 
        // you might need to sync transforms within the loop.
        UpdateTransform();
        return moveCompleted;
    }

    /**
     * Returns a list of colliders that would touch this Actor if it was moved according to the offset vector.
     * For example, if the offset was (0,-1,0), this would return a list of colliders one pixel below the Actor.
     * The default implementation reports all collisions except for self-collisions.
     * Override this to change how the actor checks for collisions when MoveAndCollide is called.
     * For example, to implement jump-through platforms, you could modify this method so it does not report a collision
     * unless the Actor is above the jump-through platform.
     * You can optionally pass in a layer mask to only check for collisions against certain layers.
     * For example, if you have a layer called "Enemies" you can pass in LayerMask.GetMask("Enemies") as the second argument.
     * Then only collisions with objects on the "Enemies" layer will be reported.
     */
    protected virtual List<Collider2D> CheckCollisions(Vector3Int offset, int layerMask = Physics2D.AllLayers) {
        // Instead of using the actual hitbox for this overlap check, we use a hitbox that is one pixel smaller in all directions.
        // The reason is that Unity's physics engine has a "contact offset" parameter which it uses to determine if colliders are overlapping.
        // If the distance between two objects is less than the contact offset, an overlap will be reported.
        // This means that if two objects are directly side-by-side (zero distance between them) but not actually overlapping, Unity will detect them as overlapping.
        // By checking a smaller hitbox and aligning everything with the pixel grid, we can work around this issue and get pixel-perfect collisions.
        Collider2D[] colliders = Physics2D.OverlapAreaAll(new Vector2Int(hitboxLeft + 1,  hitboxTop - 1)    + ((Vector2Int)offset),
                                                          new Vector2Int(hitboxRight - 1, hitboxBottom + 1) + ((Vector2Int)offset), layerMask);
        if (LayerMaskContains(gameObject.layer, layerMask)) {
            // If the object calling this function is on one of the layers we are checking, a self-collision might be reported.
            // To avoid this, we remove the object's collider from the list before returning.
            List<Collider2D> filteredColliders = new List<Collider2D>(colliders);
            filteredColliders.Remove(bc);
            return filteredColliders;
        } else {
            // Otherwise, we don't need to do any filtering. Just return the colliders we found (wrapped in a list for convenience).
            return new List<Collider2D>(colliders);
        }
    }

    /**
     * Given a list of colliders, this method allows you to implement collision response for each collider.
     * The return value determines if the Actor should stop moving as a result of a collision when used in MoveAndCollide.
     * If true, the Actor will stop. For example, you could use this to make the Actor stop when a wall is hit.
     * The default implementation makes the Actor stop if it hits a non-trigger collider, but pass through triggers.
     * It also calls the HandleCollisionWith function on each individual collider, which you can override to handle
     * collisions individually, if you prefer this to processing the whole list.
     */
    protected virtual bool HandleCollisions(List<Collider2D> colliders) {
        var stop = false;
        foreach (var collider in colliders) {
            if(!collider.isTrigger) {
                stop = true;
            }
            HandleCollisionWith(collider);
        }
        return stop;
    }

    /**
     * The default implementation of HandleCollisions will call this function on each collider in the list.
     * You can implement collision response for individual objects here.
     * For example, you could check if the collider belongs to a coin game object, and if so you could destroy the coin and increment a coin counter.
     */
    protected virtual void HandleCollisionWith(Collider2D colliders) { }

    /**
     * This is a special round function that always rounds down when the fractional part of the number is 0.5.
     * Usually it doesn't matter if you use this or the "balanced" rounding used by Unity's Mathf.Round.
     * However, this function is used in the default implementations of Move and MoveAndCollide, as it provides more consistent 
     * behavior which is desirable for some advanced uses (like implementing Actors which push or carry other Actors).
     */
    protected int RoundTiesDown(float number) {
        return Mathf.CeilToInt(number - 0.5f);
    }

    /**
     * This method computes the boundaries of the Actor's hitbox.
     * When the Actor's pixel position changes, this function should be called to keep the hitbox position in sync.
     */
    protected void ComputeHitbox() {
        hitboxLeft   = pixelPosition.x - ((int)bc.bounds.extents.x) + ((int)bc.offset.x);
        hitboxRight  = pixelPosition.x + ((int)bc.bounds.extents.x) + ((int)bc.offset.x);
        hitboxBottom = pixelPosition.y - ((int)bc.bounds.extents.y) + ((int)bc.offset.y);
        hitboxTop    = pixelPosition.y + ((int)bc.bounds.extents.y) + ((int)bc.offset.y);
    }

    /**
     * This method updates the transform of the Actor to match the Actor's pixel position.
     * It also updates Unity's internal tracking of transform positions, so that the new transform position will be reflected
     * in collision checks.
     */
    protected void UpdateTransform() {
        transform.position = pixelPosition;
        Physics2D.SyncTransforms();
    }

    /**
     * Helper function that checks if a given layer is contained in a layer mask.
     * From (https://answers.unity.com/questions/50279/check-if-layer-is-in-layermask.html)
     */
    private bool LayerMaskContains(int layer, int layerMask) {
        return layerMask == (layerMask | (1 << layer));
    }
}
