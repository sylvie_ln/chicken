﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * A simple platformer character controller. 
 */
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class PlatformerController : Actor {
    public float friction = 0.3f;
    public float speed = 1.5f;
    public float acceleration = 0.3f;
    public float gravity = 0.2f;
    public float jumpHeight = 34f;
    public Vector2 velocity = Vector2.zero;
    [HideInInspector]
    public string direction = "Right";

    private float horizontalInput = 0f;
    private bool jumpPressed = false;
    private bool jumpReleased = false;

    private Animator animator;
    protected override void Start() {
        base.Start();
        animator = GetComponent<Animator>();

        // Acceleration must exceed friction or the object will not move.
        // Also, because friction is applied every frame after capping speed, the top speed is technically "speed - friction".
        // It's annoying to account for this when specifying acceleration and speed, so we offset acceleration and speed by
        // the friction value here.
        acceleration += friction;
        speed += friction;
    }

    /** 
     * Notes on Update vs. FixedUpdate:
     * 
     * Update uses a variable timestep (runs "as often as possible" at irregular intervals).
     * FixedUpdate uses a fixed timestep (runs at regular intervals).
     * 
     * The physics system (used for collision checking) is synced with FixedUpdate rather than Update,
     * so movement and collision code should be put in FixedUpdate.
     * 
     * On the other hand, the input system is synced with Update. For example, GetButtonDown will return
     * true starting from the Update call that comes immediately after the button press, and will return false
     * after the next Update call (unless you press it again between updates, which is probably impossible
     * unless the game is lagging a lot). FixedUpdate can be called multiple times or not at all between
     * Update calls, so if you tie game logic to a GetButtonDown call in FixedUpdate, you could end up with
     * dropped inputs or have one press count as multiple presses.
     * 
     * We solve this as follows: in Update, when the jump button is pressed, we just set a variable instead
     * of doing the jump logic immediately. Then in FixedUpdate, if this variable is true, we set the variable
     * to false and then perform the jump. This ensures that when we press the jump button, the press will be
     * processed on the next FixedUpdate, and also that the press will not be processed multiple times. We handle
     * the case where the jump button is released similarly.
     */

    private void Update() {
        horizontalInput = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump")) {
            jumpPressed = true;
        }

        if (Input.GetButtonUp("Jump")) {
            jumpReleased = true;
        }
    }

    private void FixedUpdate() {
        // Accelerate according to input direction
        var horizontalMoveVector = new Vector2(horizontalInput * acceleration, 0f);
        velocity += horizontalMoveVector;

        // Apply speed cap to the velocity
        // This is a really simple way of doing it, you'll need something more advanced if you want the player's max "walking speed"
        // to be different from the max "movement speed" (like if the player gets launched really fast by something, and goes faster
        // than they can normally walk for a bit).
        if (Mathf.Abs(velocity.x) > speed) {
            velocity.x = speed * Mathf.Sign(velocity.x);
        }

        // Apply friction
        // Friction is applied at all times, even when in the air.
        if (velocity.x != 0) {
            // Get the sign of the horizontal velocity
            var sign = Mathf.Sign(velocity.x);
            // Subtract friction constant
            velocity.x -= friction * sign;
            // If the sign changed after subtracting, we set the horizontal velocity to zero
            // Otherwise subtracting the friction could make the horizontal velocity "overshoot" zero, and the object would wiggle around instead of stopping
            if (Mathf.Sign(velocity.x) != sign) {
                velocity.x = 0;
            }
        }

        // Check whether we're on ground and cache the value, so we don't do extra collision checks
        var onGround = OnGround();

        // Apply gravity
        if (!onGround) {
            velocity.y -= gravity;
        }

        // Jump (if the button is pressed and you're on the ground)
        if (ConsumeBool(ref jumpPressed) && onGround) {
            velocity.y = CalcJump(jumpHeight, gravity);
        }
        
        // Cut jump short if you release the jump button while travelling upwards, for jump height control
        if (ConsumeBool(ref jumpReleased) && velocity.y > 0) {
            velocity.y /= 2;
        }

        // Set facing direction based on last pressed horizontal movement key
        if (horizontalInput != 0) {
            if (horizontalInput > 0) {
                direction = "Right";
            } else {
                direction = "Left";
            }
        }
        
        // Move, setting velocity components to zero if we hit something
        if (!MoveAndCollide(velocity.x, 0)) { velocity.x = 0f; }
        if (!MoveAndCollide(velocity.y, 1)) { velocity.y = 0f; }


        // Animation
        var animation = "";
        if (onGround) {
            if (horizontalInput != 0) {
                animation = "Walk";
            } else {
                animation = "Stand";
            }
        } else {
            animation = "Jump";
        }
        animator.Play(animation + direction);
    }

    /**
     * Handle collisions with individual objects.
     */
    protected override void HandleCollisionWith(Collider2D collider) {
        var obj = collider.gameObject;
        var name = collider.gameObject.name;
        if (name == "Coin") {
            Destroy(obj);
        }
    }

    /**
     * Check if the character is standing on the ground or something else solid.
     */
    private bool OnGround() {
        var belowMe = CheckCollisions(Vector3Int.down, LayerMask.GetMask("Ground", "Solid Actors"));
        return belowMe.Count != 0;
    }

    /** 
     * Helper function that "consumes" a boolean value.
     * If the value is true, it sets it to false, and then returns the original value.
     * For example, this can be used with booleans representing a button press to ensure
     * the press is only processed once.
     */
    private bool ConsumeBool(ref bool value) {
        var originalValue = false;
        if (value) {
            originalValue = true;
            value = false;
        }
        return originalValue;
    }

    /**
     * Returns the vertical speed needed to jump (approximately) a certain number of pixels.
     */
    private static float CalcJump(float jumpHeightInPixels, float gravity) {
        return Mathf.Sqrt(2 * gravity * jumpHeightInPixels);
    }
}
