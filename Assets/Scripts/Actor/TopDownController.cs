﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * A simple top-down character controller.
 */
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class TopDownController : Actor {

    public float friction = 0.3f;
    public float speed = 1.5f;
    public float acceleration = 0.3f;
    public Vector2 velocity = Vector2.zero;
    [HideInInspector]
    public string direction = "Down";

    private Vector2 inputVector = Vector2.zero;

    private Animator animator;
    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();

        // Acceleration must exceed friction or the object will not move.
        // Also, because friction is applied every frame after capping speed, the top speed is technically "speed - friction".
        // It's annoying to account for this when specifying acceleration and speed, so we offset acceleration and speed by
        // the friction value here.
        acceleration += friction;
        speed += friction;
    }

    /**
     * Note about Update vs. FixedUpdate:
     * 
     * It's generally good practice to put input checking logic in Update and movement/collision logic in FixedUpdate.
     * Basically, input is processed before Update is called and physics stuff like collision resolution is processed before FixedUpdate,
     * so things will work better if you split your logic up this way.
     * In this case it doesn't matter too much, but if you are checking for button-press or button-release events rather 
     * than button-held events, it's very important to understand how the two update methods work with respect to input handling. 
     * See the PlatformerController class for a more detailed explanation.
     */

    private void Update() {
        inputVector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    private void FixedUpdate() {
        // Accelerate according to input direction
        var moveVector = inputVector.normalized * acceleration;
        velocity += moveVector;

        // Apply speed cap to the velocity
        // This is a really simple way of doing it, you'll need something more advanced if you want the player's max "walking speed"
        // to be different from the max "movement speed" (like if the player gets launched really fast by something, and goes faster
        // than they can normally walk for a bit).
        if (velocity.magnitude > speed) {
            velocity = velocity.normalized * speed;
        }

        // Apply friction
        if (velocity.magnitude != 0) {
            // Save the signs of the x and y velocity compoenents
            var signVector = new Vector2(Mathf.Sign(velocity.x), Mathf.Sign(velocity.y));
            // Subtract friction constant
            velocity -= velocity.normalized * friction;
            var newSignVector = new Vector2(Mathf.Sign(velocity.x), Mathf.Sign(velocity.y));
            // If the sign of the x or y component changed, set that component to zero
            // Otherwise subtracting the friction could make the component "overshoot" zero, and the object would wiggle around instead of stopping
            if(signVector.x != newSignVector.x) {
                velocity.x = 0;
            }
            if (signVector.y != newSignVector.y) {
                velocity.y = 0;
            }
        }

        // Move, setting velocity components to zero if we hit something
        if(!MoveAndCollide(velocity.x, 0)) { velocity.x = 0f; }
        if(!MoveAndCollide(velocity.y, 1)) { velocity.y = 0f; }

        // Animation
        var animation = "";
        if (moveVector != Vector2.zero) {
            animation += "Walk";
            if (moveVector.x > 0) {
                direction = "Right";
            }
            if (moveVector.x < 0) {
                direction = "Left";
            }
            if (moveVector.y > 0) {
                direction = "Up";
            }
            if (moveVector.y < 0) {
                direction = "Down";
            }
        } else {
            animation += "Stand";
        }
        animator.Play(animation + direction);
    }
    
    /**
     * Handle collisions with individual objects.
     */
    protected override void HandleCollisionWith(Collider2D collider) {
        var obj = collider.gameObject;
        var name = collider.gameObject.name;
        if (name == "Coin") {
            Destroy(obj);
        }
    }
}
