﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Helper class that lets you just draw a sprite at an arbitrary position without creating a GameObject or anything.
 * I don't know how good the performance is on this so I don't know if it should be used too heavily.
 * I only included it to draw the "object preview" for the Prefab Brush.
 */
public class Draw {
    public static void Sprite(Sprite sprite, float x, float y, float xScale, float yScale) {
        bool flipX = System.Math.Sign(xScale) == -1;
        bool flipY = System.Math.Sign(yScale) == -1;
        float xScaleAbs = System.Math.Abs(xScale);
        float yScaleAbs = System.Math.Abs(yScale);
        GL.PushMatrix();
        GL.LoadPixelMatrix(0, Screen.width, Screen.height, 0);
        Rect screenRect = new Rect(
            new Vector2(x - (sprite.pivot.x * xScaleAbs) + (Screen.width / 2), -y - (sprite.pivot.y * yScaleAbs) + (Screen.height / 2)),
            new Vector2(sprite.rect.width * xScaleAbs, sprite.rect.height * yScaleAbs));
        float minX = sprite.rect.x / sprite.texture.width;
        float minY = sprite.rect.y / sprite.texture.height;
        float maxX = (sprite.rect.x + sprite.rect.width) / sprite.texture.width;
        float maxY = (sprite.rect.y + sprite.rect.height) / sprite.texture.height;
        float temp;
        if (flipX) {
            temp = minX;
            minX = maxX;
            maxX = temp;
        }
        if (flipY) {
            temp = minY;
            minY = maxY;
            maxY = temp;
        }
        Rect normalizedSpriteRect = Rect.MinMaxRect(minX, minY, maxX, maxY);
        Graphics.DrawTexture(screenRect, sprite.texture, normalizedSpriteRect, 0, 0, 0, 0, new Color32(128, 128, 128, 128), null);
        GL.PopMatrix();
    }
}
