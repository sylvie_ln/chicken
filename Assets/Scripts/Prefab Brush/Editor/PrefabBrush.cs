﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This is a tilemap brush that lets you place prefabs, so that you can easily place them nicely aligned with the tile grid.
 * It's kind of janky, but better than nothing (?)
 * 
 * How to use:
 * 1. Right click in the "Project" window that shows all the files and stuff.
 * 2. Choose "Create" then "Custom Brushes" then "Prefab Brush".
 * 3. Enter a name for your prefab brush if desired.
 * 4. Select the prefab brush you created to open its Inspector window.
 * 5. Drag all the prefabs you want to be able to place with the brush onto the "Prefab Palette" field.
 * 6. Open the Tile Palette window and select the prefab brush you created.
 * 7. Now you can place prefabs on the active tilemap. Press the C key to cycle between the prefabs in the palette you set up.
 * 
 * Based on an example prefab brush by Paolo Ferri (http://www.codingwithunity.com/2018/01/tilemap-custom-brush-for-prefabs.html)
 * which itself is based on an example random prefab brush from the Unity 2D Extras repository (https://github.com/Unity-Technologies/2d-extras)
 */
namespace UnityEditor {
    [CreateAssetMenu(fileName = "Prefab Brush", menuName = "Custom Brushes/Prefab Brush")]
    [CustomGridBrush(false, true, false, "Prefab Brush")]
    public class PrefabBrush : UnityEditor.Tilemaps.GridBrush {

        public GameObject[] prefabPalette;
        public bool showPreviewInCorner = true;
        [HideInInspector]
        public int selectedPrefab = 0;

        public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position) {
            Transform current = GetObjectInCell(grid, brushTarget.transform, position);
            if (current == null) { 
                GameObject prefab = prefabPalette[selectedPrefab];
                GameObject instance = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
                Undo.RegisterCreatedObjectUndo((Object)instance, "Paint Prefabs");
                if (instance != null) {
                    instance.transform.SetParent(brushTarget.transform);
                    instance.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(new Vector3Int(position.x, position.y, 0) + new Vector3(.5f, .5f, .5f)));
                }
            }
        }

        public override void Erase(GridLayout grid, GameObject brushTarget, Vector3Int position) {
            // Do not allow editing palettes
            if (brushTarget.layer == 31)
                return;

            Transform erased = GetObjectInCell(grid, brushTarget.transform, new Vector3Int(position.x, position.y, position.z));
            if (erased != null)
                Undo.DestroyObjectImmediate(erased.gameObject);
        }

        private static Transform GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position) {
            int childCount = parent.childCount;
            Vector3 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
            Vector3 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + Vector3Int.one));
            Bounds bounds = new Bounds((max + min) * .5f, max - min);

            for (int i = 0; i < childCount; i++) {
                Transform child = parent.GetChild(i);
                if (bounds.Contains(child.position))
                    return child;
            }
            return null;
        }

    }

    [CustomEditor(typeof(PrefabBrush))]
    public class PrefabBrushEditor : UnityEditor.Tilemaps.GridBrushEditor {
        private PrefabBrush prefabBrush { get { return target as PrefabBrush; } }

        private float previewScale = 4;

        public override void OnPaintSceneGUI(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing) {
            base.OnPaintSceneGUI(gridLayout, null, position, tool, executing);

            Event e = Event.current;
            if (e.type == EventType.KeyDown && e.keyCode == KeyCode.C) {
                prefabBrush.selectedPrefab++;
                if (prefabBrush.selectedPrefab == prefabBrush.prefabPalette.Length) {
                    prefabBrush.selectedPrefab = 0;
                }
            }
            if (prefabBrush.showPreviewInCorner) { 
                GameObject selectedPrefabObject = prefabBrush.prefabPalette[prefabBrush.selectedPrefab];
                SpriteRenderer renderer = selectedPrefabObject.GetComponent<SpriteRenderer>();
                if (renderer != null) {
                    Draw.Sprite(renderer.sprite,
                        -Screen.width / 2 + (renderer.sprite.pivot.x * previewScale), -Screen.height / 2 + (renderer.sprite.pivot.y * previewScale), previewScale, previewScale);
                }
            }
        }
    }
}